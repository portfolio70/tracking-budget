import React from 'react'
import {Doughnut} from 'react-chartjs-2'


export default function DoughnutChart({expense, income}) {

	return (
		<Doughnut
			data={{
				datasets: [{ // follows order of labels below
					data: [expense, income], 
					backgroundColor: ['green','indianred'] 
				}],
				labels: [
					'Expenses',
					'Income'
				]
			}}
			redraw={false}  //prevents redrawing doughnut chart when it's rendering
		/>
		)
}