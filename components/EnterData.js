import {useContext, useState, useEffect} from 'react'
import {Row, Col, Form, Dropdown, Button} from 'react-bootstrap'
import styles from '../styles/Tracker.module.css'
import UserContext from '../UserContext'


export default function EnterData() {

	const {user} = useContext(UserContext)
	// console.log(user.id)

	// transaction type
	const [category, setCategory] = useState('')
	// if debit or credit
	const [type, setType] = useState('')
	// watcher boolean for type; true is credit and false is debit
	const [initialType, updatedType] = useState(true)

	const [description, setDescription] = useState('')
	const [amount, setAmount] = useState('')
	const [balAfterTxn, setBalAfterTxn] = useState('')

	// add new transaction
	const addTransaction = (e) => {
		e.preventDefault()
		fetch(`${process.env.NEXT_PUBLIC_API_URL}/api/users/enter-new-transaction/${user.id}`, {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json',
				Authorization: `Bearer ${localStorage.getItem('token')}`
			},
			body: JSON.stringify({
				// WHEN PUSHING TO SUBCOLLECTION, NO NEED TO PUT PUSH HERE OR THE SUBCOLLECTION NAME
						description: description,
						amount: amount,
						balanceAfterTransaction: balAfterTxn,
						name: category,
						type: type
				})
			})
		.then(response => response.json())
		.then(data => {

			if (data.message == 'Transaction added') {
				setSuccess(true)
				alert('Transaction successfully added.')
			} else {
				console.log(data.message)
			}
		})
	}

	return (
		<div>
			<Form onSubmit={(e) => addTransaction(e)}>
				<Form.Row>
					<Col>
						<Form.Group as={Col}>
							<Form.Label>Select Transaction Type</Form.Label>
							<Form.Control 
								as="select" 
								defaultValue={type}
								onChange={e => setType(e.target.value)}>
									<option>Credit</option>
									<option>Debit</option>
							</Form.Control>
						</Form.Group>
					</Col>
					<Col>
					<Form.Group as={Col}>
							<Form.Label>Select Type</Form.Label>
								<Form.Control 
									as="select" 
									defaultValue={category}
									onChange={e => setCategory(e.target.value)}>
										<option>Salary</option>
										<option>Gift</option>
										<option>Bonus</option>
										<option>Investment Payout</option>
										<option>Home loan</option>
										<option>Utilities</option>
										<option>Allowance</option>
										<option>Marketing</option>
										<option>Groceries</option>
										<option>Recreation</option>
										<option>Car / Gadget</option>
										<option>Transportation</option>
										<option>Insurance</option>
										<option>Personal Savings</option>
										<option>Joint Savings</option>
								</Form.Control>				
						</Form.Group>
					</Col>
				</Form.Row>
				<Form.Row>
					<Col>
						<Form.Group as={Col}>
							<Form.Label>Description</Form.Label>
							<Form.Control 
								type="text"
								defaultValue={description}
								onChange={e => setDescription(e.target.value)}>
							</Form.Control>
						</Form.Group>
					</Col>
				</Form.Row>
				<Form.Row>
					<Col>
						<Form.Group as={Col}>
							<Form.Label>Amount</Form.Label>
							<Form.Control 
								type="number"
								defaultValue={amount}
								onChange={e => setAmount(e.target.value)}>
							</Form.Control>
						</Form.Group>
						<Form.Group as={Col}>
							<Form.Label>Balance After Transaction</Form.Label>
							<Form.Control 
								type="number"
								defaultValue={balAfterTxn}
								onChange={e => setBalAfterTxn(e.target.value)}>
							</Form.Control>
						</Form.Group>
					</Col>
				</Form.Row>
				<Form.Row>
					<Col>
						<Button variant="secondary" type="submit">Submit</Button>
					</Col>
				</Form.Row>
			</Form>

		</div>
		)
}


/*


*/