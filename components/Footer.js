import styles from '../styles/Home.module.css'

export default function Footer() {
	return (
     <footer className={styles.footer}>
        <a
          href="mailto:pndbontia@gmail.com"
          target="_blank"
          rel="noopener noreferrer"
        >
          Powered by{' '}
          <img src="footer-logo.png" alt="Migoyati Logo" className={styles.logo} />
        </a>
      </footer>
		)
}