import Head from 'next/head'


export default function HeadData() {
	return (
		<Head>
			<meta char=""></meta>
			<title>Migoyati Saves</title>
			<link rel="icon" href="favicon-sm.png" />
			<meta name="viewport" content="initial-scale=1.0, width=device-width"/>
		</Head>
		)
}