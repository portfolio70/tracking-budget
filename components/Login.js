import {useState, useEffect, useContext} from 'react'
import {GoogleLogin} from 'react-google-login'
import {Row, Col, Form, Button, Image, Container} from 'react-bootstrap'
import Router from 'next/router'
import UserContext from '../UserContext'
import Swal from 'sweetalert2'
import styles from '../styles/Login.module.css'

export default function login(){
	const {setUser} = useContext(UserContext)

	const [email, setEmail] = useState('')
	const [password, setPassword] = useState('')
	const [isDisabled, setIsDisabled] = useState(true)

	// email and pw are not blank
	useEffect(() => {
		if(email !== '' && password !== '') {
			setIsDisabled(false)
		} else {
			setIsDisabled(true)
		}
	}, [email, password])

	// authenticate user
	function authenticateUser(e) {
		e.preventDefault(e)
		fetch(`${process.env.NEXT_PUBLIC_API_URL}/api/users/login`, {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json'
			},
			body: JSON.stringify({
				emailAddress: email,
				password: password
			})
		})
		.then(res => res.json())
		.then(data => {
			// console.log(data)
			if (data.accessToken) {
				localStorage.setItem('token', data.accessToken)
				fetch(`${process.env.NEXT_PUBLIC_API_URL}/api/users/details`, {
					headers: {
						Authorization: `Bearer ${data.accessToken}`
					}
				})
				.then(res => res.json())
				.then(data => {
					setUser({
						id: data._id,
						isAdmin: data.isAdmin
					})
					Router.push('/tracker')
				})
			} else {
					Router.push('/error')
			}
		})
	}

	// login via Google
	const captureGoogleLogin = (login) => {
		fetch(`${process.env.NEXT_PUBLIC_API_URL}/api/users/verify-google-id-token`, {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json'
			},
			body: JSON.stringify({
				tokenId: response.tokenId
			})
		})
		.then(res = res.json())
		.then(data => {
			// console.log(data)
		})
	}

	return (
	    <section className={styles.loginFields}>

	      <Row className="justify-content-md-center">
	        <Col xs lg="4">
	          <Form className="mb-4" onSubmit={e => authenticateUser(e)}>
	            <Form.Group>
	              <Form.Label>Email address:</Form.Label>
	              <Form.Control
	                type="email"
	                onChange={e => setEmail(e.target.value)}
	                value={email}
	                required/>
	            </Form.Group>
	            <Form.Group>
	              <Form.Label>Password:</Form.Label>
	              <Form.Control
	                type="password"
	                onChange={e => setPassword(e.target.value)}
	                value={password}
	                required/>
	            </Form.Group>

	            <Button className={styles.submitBtn} variant="secondary" type="submit" disabled={isDisabled}>Log in</Button>
	          </Form>

	          <GoogleLogin
	          	className={styles.GgleBtn}
	            clientId="827061658263-54ipqllarv7imumaoef33tt70pk4s66v.apps.googleusercontent.com"
	            onSuccess={captureGoogleLogin}
	            onFailure={captureGoogleLogin}
	            cookiePolicy={'single_host_origin'}
	            />
	        </Col>
	      </Row>          
	    </section>
	)
}
