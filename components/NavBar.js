import {useContext} from 'react'
import {Navbar, Nav} from 'react-bootstrap'
import Link from 'next/link'
import UserContext from '../UserContext'


export default function NavBar() {
	const {user} = useContext(UserContext)

	return (
		<>
		  <Navbar bg="dark" variant="dark" expand="lg">
		    <Navbar.Brand href="/">
		      <img
		        alt=""
		        src="/logo.png"
		        width="30"
		        height="30"
		        className="d-inline-block align-top"
		      />{' '}
		      Home
		    </Navbar.Brand>
		    <Navbar.Brand href="/profile">
		      <img
		        src="/logo.png"
		        width="30"
		        height="30"
		        className="d-inline-block align-top"
		        alt="React Bootstrap logo"
		      />My Profile
		    </Navbar.Brand>
		    <Navbar.Brand href="/chart">
		      <img
		        src="/logo.png"
		        width="30"
		        height="30"
		        className="d-inline-block align-top"
		        alt="React Bootstrap logo"
		      />Trends and Analysis
		    </Navbar.Brand>
		    <Navbar.Brand href="/logout">
		      <img
		        src="/logo.png"
		        width="30"
		        height="30"
		        className="d-inline-block align-top"
		        alt="React Bootstrap logo"
		      />Logout
		    </Navbar.Brand>

		  </Navbar>
		</>
		)
}