import {Table, Button, Container, Row, Col} from 'react-bootstrap'
import Link from 'next/link'
import {useContext, useState, useEffect} from 'react'
import UserContext from '../UserContext'
import styles from '../styles/Tracker.module.css'
import UpdateDetails from './UpdateDetails'

export default function TableData ({data, transactionID}) {

	const {user} = useContext(UserContext)
	const [transactionDetails, setTransactionDetails] = useState('')
	const [showUpdateDetails, setShowUpdateDetails] = useState(false)

	if (data.length < 1) {
		return (
			<h6>Click on Show My Transactions to get details</h6>
			)
		}

	// output specific details to that transaction once Details button is clicked
	// state to be passed to the UpdateDetails component is transaction
	const listTransactionDetails = (e) => {
		// e.preventDefault()
		// (e) is from e.target.value from onClick event of button Update
		fetch(`${process.env.NEXT_PUBLIC_API_URL}/api/users/transaction-details/${user.id}/${e}`)
		.then(res => res.json())
		.then(data => {
			// should output the specific details of that specific transaction
			// console.log(data)
			setTransactionDetails(data)
		})
	}

	const deleteTransaction = (event) => {
		fetch(`${process.env.NEXT_PUBLIC_API_URL}/api/users/delete-transaction/${user.id}/${event}`, {
			method: 'PUT',
			headers: {
				Authorization: `Bearer ${localStorage.getItem('token')}`
			}
		})
		.then(res => res.json())
		.then(data => {
			return alert('Transaction deleted.')
		})
	}

	// console.log(transactionDetails)

	let showInfo = (
		<Container>
			<UpdateDetails dataToShow={transactionDetails}/>
		</Container>
		) 

	if (transactionDetails.length < 1) {
		return (
			<Spinner animation="border" role="status">
				<span className="sr-only">Loading...</span>
			</Spinner>
			)
		} else {
		return (
			<Container fluid>
				<Container className={styles.detailPane}>
					<Row mb={2}>
						<Col>Category:</Col>
						<Col>{transactionDetails.type}</Col>
					</Row>
					<Row mb={2}>
						<Col>Name:</Col>
						<Col>{transactionDetails.name}</Col>
					</Row>
					<Row mb={2}>
						<Col>Description:</Col>
						<Col>{transactionDetails.description}</Col>
					</Row>
					<Row mb={2}>
						<Col>Amount:</Col>
						<Col>{transactionDetails.amount}</Col>
					</Row>
					<Row mb={2}>
						<Col>Timestamp:</Col>
						<Col>{transactionDetails.dateCreated}</Col>
					</Row>
					<Row mb={2}>
						<Col>
							<Button variant="secondary" onClick={() => setShowUpdateDetails(!showUpdateDetails)}>Update</Button>
							{(showUpdateDetails) ? showInfo : null}
						</Col>
						<Col>
							<Button variant="secondary" value={transactionDetails._id}onClick={event => deleteTransaction(event.target.value)}>Delete</Button>
						</Col>
					</Row>
				</Container>

			<Table striped bordered hover className={styles.transactionList} size="sm" responsive>
				<thead>
					<tr>
						<th>Name</th>
						<th>Amount</th>
						<th>Date Entered</th>
						<th></th>
					</tr>
				</thead>

			{data.map(item => {
				return(
					<tbody key={item._id}>
						<tr>
							<td>{item.name}</td>
							<td>{item.amount}</td>
							<td>{(item.dateCreated).toString().slice(0, 10)}</td>
							<td>
								<Button
									variant="secondary"
									value={item._id}
									transactions={item._id}
									onClick={e => {listTransactionDetails(e.target.value); () => setShowTransactionDetails(!showTransactionDetails)}}>Details</Button>
							</td>
						</tr>
					</tbody>
				)})
			}
			</Table>
			</Container>
		) 
	}
}
