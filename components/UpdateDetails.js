import {Row, Col, Form, Dropdown, Button} from 'react-bootstrap'
import {useContext, useState, useEffect, Component} from 'react'
import UserContext from '../UserContext'

export default function UpdateDetails({dataToShow}) {

	const {user} = useContext(UserContext)

	// category - utilities, marketing, etc
	const [updatedName, setUpdatedName] = useState('')

	// debit or credit
	const [updatedType, setUpdatedType] = useState(dataToShow.type)

	// description
	const [updatedDescription, setUpdatedDescription] = useState(dataToShow.description)
	const [updatedAmount, setUpdatedAmount] = useState(dataToShow.amount)
	const [updatedBalance, setUpdatedBalance] = useState(dataToShow.balanceAfterTransaction)

	console.log(dataToShow)
	console.log(dataToShow._id)

	const editTransaction = (e) => {
		e.preventDefault()
		fetch(`http://localhost:4000/api/users/edit-transaction/${user.id}/${dataToShow._id}`, {
			method: 'PUT',
			headers: {
				'Content-Type': 'application/json',
				Authorization: `Bearer ${localStorage.getItem('token')}`
			},
			body: JSON.stringify({
				description: updatedDescription,
				type: updatedType,
				amount: updatedAmount,
				balanceAfterTransaction: updatedBalance,
				name: updatedName
			})
		})
			.then(res => res.json())
			.then(data => {
				console.log(data)
			})
		}

	console.log(updatedAmount)
	console.log(setUpdatedAmount)

	return (
		<Form onSubmit={(e) => editTransaction(e)}>
			<Form.Row>
				<Col>
					<Form.Group as={Col}>
						<Form.Label>Update transaction type</Form.Label>
						<Form.Control
							as="select"
							defaultValue={updatedType}
							onChange={e => setUpdatedType(e.target.value)}
							>
								<option>Debit</option>
								<option>Credit</option>
						</Form.Control>
					</Form.Group>
				</Col>
			</Form.Row>

			<Form.Row>
				<Col>
					<Form.Group as={Col}>
						<Form.Label>Update name</Form.Label>
						<Form.Control
							as="select"
							defaultValue={updatedName}
							onChange={e => setUpdatedName(e.target.value)}
							>
								<option>Salary</option>
								<option>Gift</option>
								<option>Bonus</option>
								<option>Investment Payout</option>
								<option>Home loan</option>
								<option>Utilities</option>
								<option>Allowance</option>
								<option>Marketing</option>
								<option>Groceries</option>
								<option>Recreation</option>
								<option>Car / Gadget</option>
								<option>Transportation</option>
								<option>Insurance</option>
								<option>Personal Savings</option>
								<option>Joint Savings</option>
						</Form.Control>
					</Form.Group>
				</Col>
			</Form.Row>

			<Form.Row>
				<Col>
					<Form.Group as={Col}>
						<Form.Label>Update description</Form.Label>
						<Form.Control
							type="text"
							defaultValue={updatedDescription}
							onChange={e => setUpdatedDescription(e.target.value)}
							>
						</Form.Control>
					</Form.Group>
				</Col>
			</Form.Row>			

			<Form.Row>
				<Col>
					<Form.Group as={Col}>
						<Form.Label>Update amount</Form.Label>
						<Form.Control
							type="number"
							defaultValue={updatedAmount}
							onChange={e => setUpdatedAmount(e.target.value)}
							>
						</Form.Control>
					</Form.Group>
				</Col>
			</Form.Row>	

			<Form.Row>
				<Col>
					<Form.Group as={Col}>
						<Form.Label>Update balance after transaction</Form.Label>
						<Form.Control
							type="number"
							defaultValue={updatedBalance}
							onChange={e => setUpdatedBalance(e.target.value)}
							>
						</Form.Control>
					</Form.Group>
				</Col>
			</Form.Row>	

			<Form.Row>
				<Col>
					<Button variant="secondary" type="submit">Change details</Button>
				</Col>
			</Form.Row>
		</Form>

		)
}