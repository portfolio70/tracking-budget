import {Form, Col, Row, Button} from 'react-bootstrap'
import {useContext, useState, useEffect, Component} from 'react'
import styles from '../styles/Profile.module.css'
import UserContext from '../UserContext'

export default function UpdateUserDetails({info}) {

	// console.log(info)
	const {user} = useContext(UserContext)

	const [updatedFirstName, setUpdatedFirstName] = useState(info.firstName)
	const [updatedLastName, setUpdatedLastName] = useState(info.lastName)
	const [updatedMobileNumber, setUpdatedMobileNumber] = useState(info.mobileNumber)
	const [updatedEmailAddress, setUpdatedEmailAddress] = useState(info.emailAddress)
	const [password, setUpdatedPassword] = useState('')
	const [verifyPassword, setVerifyPassword] = useState('')
	const [isDisabled, setIsDisabled] = useState(true)


	useEffect(() => {
		if (updatedEmailAddress !== '') {
			setIsDisabled(false)
		} else {
			setIsDisabled (true)
		}
	}, [updatedEmailAddress])


	const updateUserInfo = (e) => {
		e.preventDefault()
		fetch(`${process.env.NEXT_PUBLIC_API_URL}/api/users/edit-user-details/${user.id}`, {
			method: 'PUT',
			headers: {
				'Content-Type' : 'application/json'
			},
			body: JSON.stringify({
				firstName: updatedFirstName,
				lastName: updatedLastName,
				emailAddress: updatedEmailAddress,
				mobileNumber: updatedMobileNumber
			})
		})
		.then(response => response.json())
		.then(data => {
			if (data.message == true) {
				alert('User profile successfully updated. Please click on View Profile again to refresh the data')
			} else {
				alert('Server error')
			}
		})
	}

	return (
		<Form onSubmit={(e) => updateUserInfo(e)}>
			<Form.Row>
				<Col>
					<Form.Group as={Col}>
						<Form.Label>First name:</Form.Label>
						<Form.Control
							type="text"
							value={updatedFirstName}
							onChange={e => setUpdatedFirstName(e.target.value)}>
						</Form.Control>
					</Form.Group>
				</Col>

				<Col>
					<Form.Group as={Col}>
						<Form.Label>Last name:</Form.Label>
						<Form.Control
							type="text"
							value={updatedLastName}
							onChange={e => setUpdatedLastName(e.target.value)}>
						</Form.Control>
					</Form.Group>
				</Col>
			</Form.Row>

			<Form.Row>
				<Col>
					<Form.Group as={Col}>
						<Form.Label>Email address:</Form.Label>
						<Form.Control
							type="text"
							value={updatedEmailAddress}
							onChange={e => setUpdatedEmailAddress(e.target.value)}>
						</Form.Control>
					</Form.Group>
				</Col>

				<Col>
					<Form.Group as={Col}>
						<Form.Label>Mobile number:</Form.Label>
						<Form.Control
							type="text"
							value={updatedMobileNumber}
							onChange={e => setUpdatedMobileNumber(e.target.value)}>
						</Form.Control>
					</Form.Group>
				</Col>
			</Form.Row>

			<Form.Row>
				<Button className={styles.button} variant="secondary" value={info._id} disabled={isDisabled} type="submit">Make changes</Button>
			</Form.Row>
		</Form>
		)
}