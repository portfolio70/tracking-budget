import {useContext, useState, useEffect, Component} from 'react'
import {Row, Col, Container, Button} from 'react-bootstrap'
import Head from './Head'
import NavBar from './NavBar'
import Footer from './Footer'
import styles from '../styles/Profile.module.css'
import UpdateUserDetails from './UpdateUserDetails'

export default function UserDetails({details}) {

	// console.log(details)

	const [userInfo, setUserInfo] = useState('')
	const [showEditForm, setShowEditForm] = useState(false)

	if (details === undefined) {
		return null
	} 

	if (details !== undefined) {
		return (
			<div className={styles.profileSummary}>
				<Row>
					<Col>
						<p>Full name:</p>
					</Col>
					<Col>
						<p>{details.firstName} {details.lastName}</p>
					</Col>
				</Row>
				<Row>
					<Col>
						<p>Email address:</p>
					</Col>
					<Col>
						<p>{details.emailAddress}</p>
					</Col>
				</Row>

				<Row>
					<Col>
						<p>Mobile number:</p>
					</Col>
					<Col>
						<p>{details.mobileNumber}</p>
					</Col>
				</Row>

				<Row>
					<Col>
						<p>Member since:</p>
					</Col>
					<Col>
						<p>{details.createdAt}</p>
					</Col>
				</Row>

				<Button variant="secondary" className={styles.button} value={details._id} onClick={() => setShowEditForm(!showEditForm)}>Edit profile</Button>
				{(showEditForm) ? <UpdateUserDetails info = {details}/> : null}

			</div>
		)
	}
}
