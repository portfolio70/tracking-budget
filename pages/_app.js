import {useState, useEffect} from 'react'
import 'bootstrap/dist/css/bootstrap.min.css'
import '../styles/globals.css'
import {Container} from 'react-bootstrap'
import Head from 'next/head'
import {UserProvider} from '../UserContext'

function MyApp({ Component, pageProps }) {
	const [user, setUser] = useState({
		id: null,
		isAdmin: null,
		firstName: null
	})

	const unsetUser = () => {
		localStorage.clear()
		setUser({
			id: null,
			isAdmin: null,
			firstName: null
		})
	}

	useEffect(() => {
		fetch(`${process.env.NEXT_PUBLIC_API_URL}/api/users/details`, {
			headers: {
				Authorization: `Bearer ${localStorage.getItem('token')}`
			}
		})
		.then(res => res.json())
		.then(data => {
			// console.log(data) //CHECK IF CONSOLE LOG FOR THIS DISAPPEARS
			if (data._id) {
				setUser({
					id: data._id,
					isAdmin: data.isAdmin,
					firstName: data.firstName
				})
			} else {
				setUser({
					id: null,
					isAdmin: null,
					firstName: null
				})
			}
		})
	},[user.id])

	return (
		<div>
			<Head>
				<meta char=""></meta>
				<title>Migoyati Budget Tracker</title>
				<meta name="viewport" content="initial-scale=1.0, width=device-width"/>
			</Head>
		  	<UserProvider value={{user, setUser, unsetUser}}>

			  		<Component {...pageProps} />

			</UserProvider>
		</div>
	)
}

export default MyApp
