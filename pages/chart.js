import {useContext, useState, useEffect, Component} from 'react'
import UserContext from '../UserContext'
import {Button, Row, Container} from 'react-bootstrap'
import styles from '../styles/Profile.module.css'
import NavBar from '../components/NavBar'
import HeadData from '../components/Head'
import Footer from '../components/Footer'
import Doughnut from '../components/Doughnut'

export default function analysis({data}) {

	// check if id populates correctly
	const {user} = useContext(UserContext)

	const [userInfo, setUserInfo] = useState('')


	// fetch transactions entered by user and use data to set state of setTransaction
	const showTransactions = (e) => {
		e.preventDefault()
		// CHANGE BACK TO PUBLIC API URL
		// users/profile/${user.id}
		fetch(`${process.env.NEXT_PUBLIC_API_URL}/api/users/profile/${user.id}`)
		.then(res => res.json())
		.then(data => {
			// console.log(data.transactions) //checking if this is the one that outputs to console
			setUserInfo(data.transactions)
			}
		)
	}

	console.log(userInfo.length)

	if (userInfo.length < 1) {
		return (
		<div className={styles.profilePage}>
			<HeadData/>

			<NavBar/>
			<Container fluid>
			<Row>
			<Button className={styles.button} variant="secondary" onClick={e => showTransactions(e)}>How am I doing?</Button>
			</Row>
			

			</Container>
			<Footer/>
		</div>
		)
	} else {

	const totalAmount = userInfo.map(a => a.amount)
	// console.log(totalAmount)

	const totalLogged = totalAmount.reduce(function(a,b) {
		return a + b;
	},0)

	const totalCredit = userInfo.filter(amount => amount.type === 'Credit')
	let totalCreditAmount = 0;
	totalCredit.forEach(obj => {
		for (let item in obj) {
			if (item == 'amount')
				totalCreditAmount += obj[item]
		} 
	})

	const totalDebit = userInfo.filter(amount => amount.type === 'Debit')
	let totalDebitAmount = 0;
	totalDebit.forEach(debit => {
		for (let expense in debit) {
			if (expense == 'amount')
				totalDebitAmount += debit[expense]
		}
	})

	// console.log(totalLogged)
	// console.log(totalCreditAmount)
	// console.log(totalDebitAmount)


	return (
		<div className={styles.profilePage}>
			<HeadData/>

			<NavBar/>

			<Container>
			<Row>
			<Button className={styles.button} variant="secondary" onClick={e => showTransactions(e)}>How am I doing?</Button>
			</Row>

			<h4>As of today, you've logged a total of PhP{(totalLogged).toFixed(2)}</h4>

			<Doughnut className={styles.donut} expense={totalDebitAmount} income={totalCreditAmount}/>

			</Container>

			<Footer/>
		</div>
		)
	}
}