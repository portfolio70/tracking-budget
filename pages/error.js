import Router from 'next/router'
import Link from 'next/link'
import {Row, Col, Container, Button} from 'react-bootstrap'
import styles from '../styles/Home.module.css'
import Head from '../components/Head'
import Footer from '../components/Footer'

export default function Error() {
	return (
		<>
		<html className={styles.error}>
			<Head/>
		
			<Container fluid>
				<Row>
					<Col className="mt-4">
						<h2>Oops! Something went wrong!</h2>
						<h6>Your username or password may have been incorrect.</h6>
					</Col>
				</Row>
				<Row>
					<Col>
						<Button variant="secondary" href="login" className={styles.button}>Go back to login page</Button>
					</Col>
					<Col>
						<Button variant="secondary" href="https://www.google.com" className={styles.button}>I changed my mind</Button>
					</Col>
				</Row>
				<Row>
					<Col>
						<img src="/error.jpg" className={styles.errorPage}></img>
					</Col>
				</Row>
			</Container>

			<Footer/>
		</html>
		</>
		)
}