import {useState, useEffect, useContext} from 'react'
import {GoogleLogin} from 'react-google-login'
import styles from '../styles/Home.module.css'
import Footer from '../components/Footer'
import Login from '../components/Login'
import Register from '../components/Register'
import {Button, Row, Col, Form, Container} from 'react-bootstrap'
import Link from 'next/router'
import UserContext from '../UserContext'
import Swal from 'sweetalert2'
import HeadData from '../components/Head'
import Router from 'next/router'

export default function Home({}) {

  const {setUser} = useContext(UserContext)
  const [showLogin, setShowLogin] = useState(false)

  let showLoginForm = (
    <Container>
      <Login/>
    </Container>
    )


  return (
      <div className={styles.container}>
        <HeadData/>

        <main className={styles.main}>
          <h1 className={styles.title}>
            The Giant Budget Tracker Project
          </h1>

          <Button className={styles.button} variant="secondary" type="submit" onClick={() => setShowLogin(!showLogin)}>Log in</Button>
          {(showLogin) ? showLoginForm : null}

          <Button 
            className={styles.button} 
            variant="secondary" 
            onClick={(e) => {
              e.preventDefault();
              window.location.href='/register';
            }}>Register</Button>

        </main>

      <Footer className={styles.footer}/>
      </div>
  )
}
