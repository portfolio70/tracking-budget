import {useState, useEffect, useContext} from 'react'
import {GoogleLogin} from 'react-google-login'
import {Row, Col, Form, Button, Image, Container} from 'react-bootstrap'
import Router from 'next/router'
import UserContext from '../UserContext'
import Swal from 'sweetalert2'
import styles from '../styles/Login.module.css'
import Footer from '../components/Footer'
import HeadData from '../components/Head'

export default function loginPage() {
	const {setUser} = useContext(UserContext)

	const [email, setEmail] = useState('')
	const [password, setPassword] = useState('')
	const [isDisabled, setIsDisabled] = useState(true)

	// email and pw are not blank
	useEffect(() => {
		if(email !== '' && password !== '') {
			setIsDisabled(false)
		} else {
			setIsDisabled(true)
		}
	}, [email, password])

	// authenticate user
	function authenticateUser(e) {
		e.preventDefault(e)
		// -------- CHANGE -----------------
		fetch(`${process.env.NEXT_PUBLIC_API_URL}/api/users/login`, {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json'
			},
			body: JSON.stringify({
				emailAddress: email,
				password: password
			})
		})
		.then(res => res.json())
		.then(data => {
			// console.log(data)
			if (data.accessToken) {
				localStorage.setItem('token', data.accessToken)
				// `${process.env.NEXT_PUBLIC_API_URL}/api/users/login`
				// old URL: http://localhost:4000/api/users/details
				fetch(`${process.env.NEXT_PUBLIC_API_URL}/api/users/details`, {
					headers: {
						Authorization: `Bearer ${data.accessToken}`
					}
				})
				.then(res => res.json())
				.then(data => {
					setUser({
						id: data._id,
						isAdmin: data.isAdmin
					})
					localStorage.setItem("id", data._id)
					Router.push('/tracker')
				})
			} else {
				Router.push('error')
			}
		})
	}

	// login via Google
	const captureGoogleLogin = (response) => {
		// `${process.env.NEXT_PUBLIC_API_URL}/api/users/details`
		// old URL http://localhost:4000/api/users/verify-google-id-token
		// --------------- CHANGE ----------------------
		fetch(`${process.env.NEXT_PUBLIC_API_URL}/api/users/verify-google-id-token`, {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json'
			},
			body: JSON.stringify({tokenId: response.tokenId})
		})
		.then(res => res.json())
		.then(data => {
			// console.log(data)
			if (typeof data.accessToken !== 'undefined') {
				localStorage.setItem('token', data.accessToken)
				setUser({id: data._id, isAdmin: data.isAdmin})
				Router.push('/tracker')
			} else {
				if (data.error == 'login-type-error') {
					Swal.fire(
						'Login Type Error',
						'You may have registered through a different login procedure',
						'error'
						)
				} else if (data.error == 'google-auth-error') {
					Swal.fire(
						'Google Auth Error',
						'Google authentication procedure failed.',
						'error'
						)			
				}
			}
		})
	}

	return (
		<div className={styles.loginPage}>
        	<HeadData/>

			<Row className="justify-content-md-center mt-5">
				<h2>Welcome back to tracking your budget.</h2>
			</Row>

			<Row className="justify-content-md-center" >
				<Col xs={6} md={4} className="ml-4 mt-3 mb-5">
					<Form className={styles.loginForm} onSubmit={e => authenticateUser(e)}>
						<Form.Group>
							<Form.Label>Email address:</Form.Label>
							<Form.Control
								type="email"
								onChange={e => setEmail(e.target.value)}
								value={email}
								required/>
						</Form.Group>
						<Form.Group>
							<Form.Label>Password:</Form.Label>
							<Form.Control
								type="password"
								onChange={e => setPassword(e.target.value)}
								value={password}
								required/>
						</Form.Group>

						<Button className={styles.submitBtn} variant="secondary" type="submit" disabled={isDisabled}>Log in</Button>
					</Form>

					<GoogleLogin
						clientId="827061658263-54ipqllarv7imumaoef33tt70pk4s66v.apps.googleusercontent.com"
						onSuccess={captureGoogleLogin}
						onFailure={captureGoogleLogin}
						cookiePolicy={'single_host_origin'}
						/>
				</Col>
			</Row>

			<Footer/>

		</div>
		)
}