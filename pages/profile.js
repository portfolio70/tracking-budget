import {useState, useEffect, useContext} from 'react'
import {Button, Row, Col, Form, Container} from 'react-bootstrap'
import Footer from '../components/Footer'
import Router from 'next/router'
import UserContext from '../UserContext'
import Swal from 'sweetalert2'
import HeadData from '../components/Head'
import styles from '../styles/Profile.module.css'
import UpdateDetails from '../components/UpdateDetails'
import UserDetails from '../components/UserDetails'
import NavBar from '../components/NavBar'




export default function Profile(props) {

	const {user} = useContext(UserContext)
	// console.log(props)

	// if condition for showing updateProfile component
	const [updateProfile, setUpdateProfile] = useState(false)
	// watcher for routing to deactivate page
	const [deactivateUser, setDeactivateUser] = useState(false)
	// array assignment that'll contain user details
	const [userInfo, setUserInfo] = useState()

	let showUpdateForm = (
		<Container>
			<UpdateDetails/>
		</Container>
		)

	// fetch profile for filling out
	// fetch transactions entered by user and use data to set state of setTransaction
	const showDetails = () => {
		fetch(`${process.env.NEXT_PUBLIC_API_URL}/api/users/profile/${user.id}`)
		.then(res => res.json())
		.then(data => {
			// REMOVE ONCE OKAY
			// console.log(data)
			setUserInfo(data)
			}
		)
	}

	const deactivateUserAccount = (event) => {
		// event.preventDefault()
		fetch(`${process.env.NEXT_PUBLIC_API_URL}/api/users/deactivate-user/${user.id}`, {
			method: 'PUT',
			headers: {
				'Content-Type': 'application/json'
			}
		})
		.then(response => response.json())
		.then((data) => {
			if (data) {
				Router.push('/logout')
			} else {
				alert('Something went wrong')
			}
		})

	}

	let showUserDetails = (
		<Container>
			<UserDetails details={userInfo}/>
		</Container>
		)

	return (
		<div>
		<HeadData/>

		<NavBar/>
		<Container fluid className={styles.profilePage}>
			<h4>Welcome to your home page, {user.firstName}.</h4>
			<Row>
				<Col>
			<Button className={styles.button} variant="secondary" onClick={e => showDetails(e.target.value)}>View profile</Button>
				</Col>
			</Row>

			{showUserDetails}

			<Row>
				<Col>
			<Button className={styles.button} variant="secondary" onClick={event => deactivateUserAccount(event.target.value)}>Deactivate account</Button>
				</Col>
			</Row>
		</Container>

		<Footer/>
		</div>
		)
	}




