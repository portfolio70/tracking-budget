import {useState, useEffect} from 'react'
import {Row, Col, Form, Button} from 'react-bootstrap'
import Router from 'next/router'
import Swal from 'sweetalert2'
import styles from '../styles/Login.module.css'
import HeadData from '../components/Head'
import Footer from '../components/Footer'

export default function register() {

	const [email, setEmail] = useState('')
	const [password, setPassword] = useState('')
	const [verifyPassword, setVerifyPassword] = useState('')
	const [firstName, setFirstName] = useState('')
	const [lastName, setLastName] = useState('')
	const [mobileNumber, setMobileNumber] = useState('')

	const [isDisabled, setIsDisabled] = useState(true)

	useEffect(() => {
		if ((password !== '' && verifyPassword !== '') && (password === verifyPassword)) {
			setIsDisabled(false)
		} else {
			setIsDisabled(true)
		}
	}, [password, verifyPassword])

	function registerUser(e) {
		e.preventDefault(e)
		// old URL: http://localhost:4000/api/users/register
		fetch(`${process.env.NEXT_PUBLIC_API_URL}/api/users/register`, {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json'
			},
			body: JSON.stringify({
				firstName: firstName,
				lastName: lastName,
				emailAddress: email,
				password: password,
				mobileNumber: mobileNumber,
				loginType: 'manual'
			})
		})
		.then(res => res.json())
		.then(data => {
			if (data.error) {
				if (data.error == 'use-google-login') {
					Swal.fire(
						'Google Account Used',
						'You are already registered with your Google account.',
						'error')
				} else if (data.error == 'account-already-registered') {
					Swal.fire(
						'Already registered',
						'Email already registered. Please <a href="/login">log in instead</a>',
						'error'
						)
				}
			} else {
				Router.push('/login')
			}
		})
	}

	return (
		<div className={styles.loginPage}>
			
		<HeadData/>

		<Row>
			<Col md={8} className="mx-auto">
				<Form onSubmit={(e) => registerUser(e)} className={styles.registerForm}>
					<Form.Group>
						<Form.Label>First Name:</Form.Label>
						<Form.Control
							type="text"
							onChange={e => setFirstName(e.target.value)}
							value={firstName}
							required/>
					</Form.Group>
					<Form.Group>
						<Form.Label>Last Name:</Form.Label>
						<Form.Control
							type="text"
							onChange={e => setLastName(e.target.value)}
							value={lastName}
							required/>
					</Form.Group>
					<Form.Group>
						<Form.Label>Email address:</Form.Label>
						<Form.Control
							type="email"
							onChange={e => setEmail(e.target.value)}
							value={email}
							required/>
					</Form.Group>
					<Form.Group>
						<Form.Label>Password:</Form.Label>
						<Form.Control
							type="password"
							onChange={e => setPassword(e.target.value)}
							value={password}
							required/>
					</Form.Group>
					<Form.Group>
						<Form.Label>Verify password:</Form.Label>
						<Form.Control
							type="password"
							onChange={e => setVerifyPassword(e.target.value)}
							value={verifyPassword}
							required/>
					</Form.Group>
					<Form.Group>
						<Form.Label>Mobile number:</Form.Label>
						<Form.Control
							type="text"
							onChange={e => setMobileNumber(e.target.value)}
							value={mobileNumber}
							required/>
					</Form.Group>

					<Button variant="secondary" type="submit" disabled={isDisabled}>Register
					</Button>
				</Form>
			</Col>
		</Row>

		<Footer/>
		</div>
		)
}