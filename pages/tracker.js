// parents should always have the functions to pass to the child components
import {useContext, useState, useEffect, Component} from 'react'
import UserContext from '../UserContext'
import NavBar from '../components/NavBar'
import HeadData from '../components/Head'
import Footer from '../components/Footer'
import EnterData from '../components/EnterData'
import {Form, Button, Table, Container, Col, Row} from 'react-bootstrap'
import ExpenseDash from '../components/ExpenseDash'
import styles from '../styles/Tracker.module.css'
import Link from 'next/link'
import UpdateDetails from '../components/UpdateDetails'
import TableData from '../components/TableData'


export default function tracker({data, transactionDetails}) {

	// check if id populates correctly
	const {user} = useContext(UserContext)
	// console.log(user.id)

	// show/hide transaction form
	const [showTransactionForm, setShowTransactionForm] = useState(false)

	// show hide transaction details
	const [showTransactionDetails, setShowTransactionDetails] = useState(false)

	// store data for fetch user transactions
	const [transaction, setTransaction] = useState([])

	// success monitor for posting new transaction in db
	const [success,setSuccess] = useState(false)

	// will contain the details of that specific transaction ID when it is clicked
	const [transactionID, setTransactionID] = useState('')

	// fetch transactions entered by user and use data to set state of setTransaction
	const showTransactions = (e) => {
		e.preventDefault()
		fetch(`${process.env.NEXT_PUBLIC_API_URL}/api/users/profile/${user.id}`)
		.then(res => res.json())
		.then(data => {
			// console.log(data.transactions) //checking if this is the one that outputs to console
			setTransaction(data.transactions)
			}
		)
	}
	
	// output specific details to that transaction once Details button is clicked
	// state to be passed to the UpdateDetails component is transaction
	const listTransactionDetails = (e) => {
		// e.preventDefault()
		// (e) is from e.target.value from onClick event of button Update
		fetch(`${process.env.NEXT_PUBLIC_API_URL}/api/users/transaction-details/${user.id}/${e}`)
		.then(res => res.json())
		.then(data => {
			// should output the specific details of that specific transaction
			// console.log(data)
			setTransactionID(data)
		})
	}

	// update specific transaction details upon clicking of Update button in outputTransactionDetails
	const updateSpecificTransaction = (event) => {
		fetch(`${process.env.NEXT_PUBLIC_API_URL}/api/users/edit-transaction/${user.id}/${e}`, {
			method: 'PUT',
			headers: {
				'Content-Type': 'application/json',
				Authorization: `Bearer ${localStorage.getItem('token')}`
			},
			body: JSON.stringify({
				description: updatedDescription,
				type: updatedType,
				amount: updatedAmount,
				balanceAfterTransaction: updatedBalanceAfterTransaction,
				name: updatedName
			})
			.then(res => res.json())
			.then(data => {
				console.log(data)

			})
		}
	)}

	let showTheTransactionForm = (
		<Container>
			<EnterData/>
		</Container>
		)

	let outputTransactionDetails = (
		<Container className={styles.detailPane}>
			<Row mb={2}>
				<Col>Category:</Col>
				<Col>{transactionID.type}</Col>
			</Row>
			<Row mb={2}>
				<Col>Name:</Col>
				<Col>{transactionID.name}</Col>
			</Row>
			<Row mb={2}>
				<Col>Description:</Col>
				<Col>{transactionID.description}</Col>
			</Row>
			<Row mb={2}>
				<Col>Amount:</Col>
				<Col>{transactionID.amount}</Col>
			</Row>
			<Row mb={2}>
				<Col>Timestamp:</Col>
				<Col>{transactionID.dateCreated}</Col>
			</Row>
			<Row mb={2}>
				<Col>
					<Button variant="secondary">Update</Button>
				</Col>
				<Col>
					<Button variant="secondary">Delete</Button>
				</Col>
			</Row>
		</Container>
		)

	// console.log(showTransactionDetails)
	// console.log(transactionID)

	return (

			<div className={styles.trackerPage}>

				<NavBar/>

				<h3 className={styles.welcome}>Welcome, {user.firstName}.</h3>

				<Col md={8}>
				<Button variant="secondary" onClick={() => setShowTransactionForm(!showTransactionForm)} className={styles.button}>Add new transaction</Button>

				{(showTransactionForm) ? showTheTransactionForm : null}

				</Col>

				<Col md={8}>
					<Button variant="secondary" onClick={e => showTransactions(e)} className={styles.button}>Show my transactions</Button>
				</Col>

				<TableData data = {transaction} details = {transactionID}/>

				<Footer/>

			</div>
	)
}
