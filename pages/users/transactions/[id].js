import {useRouter} from 'next/router'
import {useContext, useState, useEffect} from 'react'
import UserContext from '../../../UserContext'
import tracker from '../../tracker'
import {Button} from 'react-bootstrap'

export default function transactionDetails({tracker, tableData}) {

	const {user} = useContext(UserContext)

	const listTransactions = (e) => {
		e.preventDefault()
		fetch(`${process.env.NEXT_PUBLIC_API_URL}/api/users/transactions/${user.id}`)
		.then(res => res.json())
		.then(data => {
			// console.log(data)
		})
	}

	return (
		<div>
			<h2>Test uli</h2>
			<Button variant="secondary" onClick={e => {listTransactions(e)}}>Click me</Button>
		</div>
		)
}